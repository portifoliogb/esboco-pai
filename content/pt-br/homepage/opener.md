---
title: "Seu melhor amigo em mãos experientes."
header_menu_title: "Sobre mim"
navigation_menu_title: "Sobre mim"
weight: 1
---
##### Dr. Vanner atua **há mais de 30 anos** como médico veterinário.
O seu pet estará nas mãos experientes e sob os olhos atenciosos de um médico veterinário com mais de três décadas de experiência, incluindo como professor pela Universidade de Brasília (UnB) e doutor pela Universidade de São Paulo (USP).
